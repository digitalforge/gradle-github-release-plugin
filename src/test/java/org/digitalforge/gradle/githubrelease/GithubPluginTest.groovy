package org.digitalforge.gradle.githubrelease

import org.gradle.testfixtures.ProjectBuilder

public class GithubPluginTest {

    private Project project;

    @Before
    void setUp() {
        project = ProjectBuilder.builder().build();
        project.apply plugin: 'org.digitalforge.githubrelease'
    }

    @Test
    void testIsGithubReleaseTask() {
        Assertions.assertTrue(project.getTasks().findByName("githubRelease") instanceof GithubReleaseTask)
    }

}

