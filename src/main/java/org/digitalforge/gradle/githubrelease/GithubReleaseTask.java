package org.digitalforge.gradle.githubrelease;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.Map;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.FileEntity;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.gradle.api.DefaultTask;
import org.gradle.api.GradleScriptException;
import org.gradle.api.tasks.TaskAction;
import org.zeroturnaround.zip.ZipUtil;

import org.digitalforge.sneakythrow.SneakyThrow;

public class GithubReleaseTask extends DefaultTask {

    private static final String USER_AGENT = "gradle-github-plugin";
    private static final ObjectMapper mapper = new ObjectMapper();

    @TaskAction
    public void release() {
        
        GithubReleaseExtension options = getProject().getExtensions().findByType(GithubReleaseExtension.class);

        CloseableHttpClient httpClient = HttpClients.custom()
            .setUserAgent(USER_AGENT)
            .build();

        String path = options.getBaseUrl() + "/repos/" + options.getOwner() + "/" + options.getRepo() + "/releases";
        HttpPost post = new HttpPost(path);

        Map<String, Object> postMap = Map.of(
                "tag_name",         options.getTagName(),
                "target_commitish", options.getTargetCommitish(),
                "name",             options.getName(),
                "body",             options.getBody(),
                "prerelease",       options.isPrerelease(),
                "draft",            options.isDraft()
        );

        post.setHeader("Content-Type", ContentType.APPLICATION_JSON.toString());
        post.setHeader("Accept", options.getAcceptHeader());
        post.setHeader("Authorization", "Token " + options.getToken());

        String body;
        try {
            body = mapper.writeValueAsString(postMap);
        } catch(JsonProcessingException ex) {
            throw SneakyThrow.sneak(ex);
        }

        post.setEntity(new StringEntity(body));

        String postLogMessage = "POST " + post.getPath() + "\n" +
            " > User-Agent: " + USER_AGENT + "\n" +
            " > Authorization: (not shown)\n" +
            " > Accept: " + options.getAcceptHeader() + "\n" +
            " > body: " + body + "\n";

        getLogger().debug(postLogMessage);

        try(CloseableHttpResponse response =  httpClient.execute(post)) {

        if((response.getCode() / 100) == 2) {
            getLogger().debug("< " + response.getCode() + " " + response.getReasonPhrase());
            getLogger().debug("Response headers:\n" + createHeaderLogString(response.getHeaders()));
            if(options.getAssets() != null) {
                Map<String, Object> json;
                try {
                    json = mapper.readValue(response.getEntity().getContent(), Map.class);
                } catch(IOException ex) {
                    throw SneakyThrow.sneak(ex);
                }
                postAssets(httpClient, options, (String)json.get("upload_url"));
            }
        } else {
            getLogger().error("Error in " + postLogMessage);
            getLogger().debug("Response headers:\n" + createHeaderLogString(response.getHeaders()));
            String respStr;
            try {
                respStr = EntityUtils.toString(response.getEntity());
            } catch(IOException | ParseException ex) {
                throw SneakyThrow.sneak(ex);
            }
            throw new GradleScriptException(respStr, null);
        }

        } catch(IOException ex) {
            throw SneakyThrow.sneak(ex);
        }

    }

    public void postAssets(CloseableHttpClient httpClient, GithubReleaseExtension options, String uploadUrl) {

        Stream.of(options.getAssets()).parallel().forEach((asset) -> {

            File file = new File(asset);
            String name = file.getName();
            if(file.exists() && file.isDirectory()) {
                name += ".zip";
            }

            String upload = uploadUrl.replace("{?name,label}", "?name=" + name + "&label=" + name);

            getLogger().debug("Upload URL: " + upload);

            if(file.exists()) {

                if(file.isDirectory()) {

                    try {

                        File tmp = Files.createTempFile("", "").toFile();

                        ZipUtil.pack(file, tmp);

                        file = tmp;

                    } catch(IOException ex) {
                        throw SneakyThrow.sneak(ex);
                    }

                }

                HttpPost post = new HttpPost(upload);

                String contentType = URLConnection.getFileNameMap().getContentTypeFor(file.toString());

                post.setHeader("Content-Type", contentType);
                post.setHeader("Accept", options.getAcceptHeader());
                post.setHeader("Authorization", "Token " + options.getToken());

                post.setEntity(new FileEntity(file, ContentType.APPLICATION_OCTET_STREAM));

                try(CloseableHttpResponse response = httpClient.execute(post)) {

                    String json = EntityUtils.toString(response.getEntity());

                    if((response.getCode() / 100) == 2) {
                        getLogger().debug(json);
                    } else {
                        getLogger().error(json);
                    }

                    if(file.exists() && file.getName().contains(".zip")) {
                        file.delete();
                    }

                } catch(IOException | ParseException ex) {
                    throw SneakyThrow.sneak(ex);
                }

            }

        });

    }

    public String createHeaderLogString(Header[] headers) {

        StringBuilder respHeaderStr = new StringBuilder();
        for(Header h : headers) {
            respHeaderStr.append("  ");
            respHeaderStr.append(h.getName());
            respHeaderStr.append(": ");
            respHeaderStr.append(!h.isSensitive() ? h.getValue() : "****************");
            respHeaderStr.append('\n');
        }

        return headers.toString();

    }
}

