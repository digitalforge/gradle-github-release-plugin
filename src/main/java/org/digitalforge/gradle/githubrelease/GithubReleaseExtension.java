package org.digitalforge.gradle.githubrelease;

public class GithubReleaseExtension {

    private String baseUrl = "https://api.github.com";
    private String acceptHeader = "application/vnd.github.v3+json";
    private String owner;
    private String repo;
    private String token;
    private String tagName;
    private String targetCommitish = "master";
    private String name;
    private String body;

    private boolean prerelease = false;
    private boolean draft = false;

    private String[] assets;

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getAcceptHeader() {
        return acceptHeader;
    }

    public void setAcceptHeader(String acceptHeader) {
        this.acceptHeader = acceptHeader;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        if(owner == null || owner.isEmpty()) {
            throw new IllegalArgumentException("owner");
        }
        this.owner = owner;
    }

    public String getRepo() {
        return repo;
    }

    public void setRepo(String repo) {
        if(repo == null || repo.isEmpty()) {
            throw new IllegalArgumentException("repo");
        }
        this.repo = repo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        if(token == null || token.isEmpty()) {
            throw new IllegalArgumentException("token");
        }
        this.token = token;
    }

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public String getTargetCommitish() {
        return targetCommitish;
    }

    public void setTargetCommitish(String targetCommitish) {
        this.targetCommitish = targetCommitish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public boolean isPrerelease() {
        return prerelease;
    }

    public void setPrerelease(boolean prerelease) {
        this.prerelease = prerelease;
    }

    public boolean isDraft() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft = draft;
    }

    public String[] getAssets() {
        return assets;
    }

    public void setAssets(String[] assets) {
        this.assets = assets;
    }

}
