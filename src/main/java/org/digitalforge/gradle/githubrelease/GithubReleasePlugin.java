package org.digitalforge.gradle.githubrelease;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class GithubReleasePlugin implements Plugin<Project> {

    @Override
    public void apply(Project project) {
        project.getExtensions().create("github", GithubReleaseExtension.class);
        project.getTasks().create("githubRelease", GithubReleaseTask.class);
    }

}